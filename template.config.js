module.exports = {
  placeholderName: 'ReactOnce',
  titlePlaceholder: 'Made with React Once',
  templateDir: './template',
}
