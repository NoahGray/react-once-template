module.exports = (api) => {
  api.cache(false)

  return {
    presets: ['babel-preset-expo'],
    plugins: [
      [
        'module:react-native-dotenv',
        {
          moduleName: 'react-native-dotenv',
          allowUndefined: true,
          safe: true,
        },
      ],
      ['@babel/plugin-proposal-decorators', {legacy: true}],
      [
        'module-resolver',
        {
          root: ['./'],
          alias: {
            components: './src/components',
            store: './src/store',
            static: './src/static',
            common: './src/components/common',
            'mock-data': './src/mock-data',
            'package.json': './package.json',
            public: './src/public',
            // 'react-native-vector-icons': 'react-web-vector-icons',
          },
        },
      ],
      // '@babel/plugin-proposal-class-properties', // Breaks fast refresh on native for some reason, left here if needed
    ],
  }
}
