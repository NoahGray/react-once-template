/**
 * Metro configuration for React Native
 * https://github.com/facebook/react-native
 *
 * @format
 */

module.exports = {
  resolver: {
    extraNodeModules: {
      components: './src/components',
      pages: './src/pages',
      store: './src/store',
      public: './src/public',
      common: './src/components/common',
      'mock-data': './src/mock-data',
    },
  },
  transformer: {
    getTransformOptions: async () => ({
      transform: {
        experimentalImportSupport: false,
        inlineRequires: false,
      },
    }),
  },
}
