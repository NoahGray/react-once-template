/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react'
import {TouchableOpacity, SafeAreaView, Text} from 'react-native'
import * as Font from 'expo-font'
import 'react-native-gesture-handler'
import {useRouting} from 'expo-next-react-navigation'
import {NavigationContainer} from '@react-navigation/native'
import {createStackNavigator} from '@react-navigation/stack'
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs'
import Header from 'components/Header'
import Feed from 'components/Feed'
import User from 'components/User'
import TabBar from 'components/TabBar'
import Login from 'common/Login'
import print from 'components/labels'
import styled from 'styled-components/native'
import {Link} from 'expo-next-react-navigation'
import wrapper from 'store'
import {NEXT_STATIC_STORAGE_ENDPOINT, API_HOST} from 'react-native-dotenv'

const Stack = createStackNavigator()
const RootStack = createStackNavigator()
const Tabs = createBottomTabNavigator()

const WebBackdrop = styled.View`
  background-color: black;
  position: absolute;
  padding-top: 100px;
`

const BackComponent = styled(Link)`
  flex-direction: row;
  margin-top: 50px;
  margin-left: 30px;
`

const BackContainer = styled.View`
  width: 100%;
  height: 100;
  background-color: black;
`

const BigB = styled.Text`
  color: white;
  font-size: 40;
  font-family: VT323;
  justify-content: center;
  padding-right: 20px;
`

const BackText = styled.Text`
  font-family: lotus;
  font-size: 30;
  color: white;
  margin-left: 10px;
`

const BottomTabs = () => {
  const router = useRouting()

  return (
    <Tabs.Navigator
      initialRouteName="/feed"
      tabBarOptions={{
        mode: 'screen',
      }}
      tabBar={(props) => (
        <TabBar
          {...props}
          router={router}
          style={{overflow: 'hidden', flexDirection: 'column'}}
        />
      )}
      router={router}
    >
      <Stack.Screen name="/feed" component={Feed} router={router} />
      {/* <Stack.Screen name="/dream" component={Dream} router={router} /> */}
      {/* <Stack.Screen name="/user" component={User} router={router} /> */}
    </Tabs.Navigator>
  )
}

// const Modal = (props) => {
//   const router = useRouting()
//
//   return (
//       <Stack.Screen
//         name="/login"
//         component={Login}
//         {...props}
//         router={router}
//       />
//   )
// }

function ModalScreen() {
  const router = useRouting()
  return (
    <SafeAreaView
      style={{
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        paddingHorizontal: 20,
      }}
    >
      <Login router={router} />
    </SafeAreaView>
  )
}

const App = (props) => {
  const [isFontsLoaded] = Font.useFonts({
    icomoon: require('./public/fonts/icons/icomoon.ttf'),
    lotus: require('./public/fonts/lotus.ttf'),
  })

  return isFontsLoaded ? (
    <NavigationContainer
      theme={{
        colors: {background: '#2d292d'},
      }}
    >
      <RootStack.Navigator
        initialRouteName="/index"
        headerTitle="Tiny"
        headerMode="screen"
        // screenOptions={({route, navigation}) => ({
        //   cardOverlayEnabled: true,
        //   header: Header,
        // })}
        screenOptions={({route, navigation}) => ({
          cardOverlayEnabled: true,
          header: (props) => {
            if (props.scene.route.name !== '/login') {
              return <Header {...props} />
            }

            return (
              <BackContainer>
                <BackComponent routeName="/index" shallow scroll={false}>
                  <BackText>{print('Back')}</BackText>
                </BackComponent>
              </BackContainer>
            )
          },
        })}
      >
        <RootStack.Screen name="/index" component={BottomTabs} {...props} />
        <RootStack.Screen
          theme={{
            colors: {background: '#000000'},
          }}
          name="/login"
          component={ModalScreen}
          {...props}
        />
      </RootStack.Navigator>
    </NavigationContainer>
  ) : null
}

export default wrapper.withRedux(App)
