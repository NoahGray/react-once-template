// @flow

import React, {Component} from 'react'
import {getDreams, clearPlatforms} from 'store/modules/dreams'
import {bindActionCreators} from 'redux'
import {NextSeo} from 'next-seo'
import wrapper from 'store'

import Feed from 'components/Feed'
import Welcome from 'components/Welcome'

type FeedType = {
  router: Object,
  dreams: Array,
  user: Object,
}

export default class UserFeed extends Component<FeedType> {
  render() {
    const {
      dreams,
      router: {
        query: {user: pageOfUser},
      },
      user,
    } = this.props

    const pageTitle = `${
      dreams.length !== 0 ? dreams[0].user.name : pageOfUser
    }'s page on ReactOnce`

    const isBlankFeed = !dreams || dreams.length === 0
    const isOwnFeed = user && pageOfUser === user.name

    return (
      <>
        <NextSeo
          title={pageTitle}
          description="One of the great platforms you can develop for with React."
        />
        {isBlankFeed ? (
          <Welcome isOwnFeed={isOwnFeed} />
        ) : (
          <Feed {...this.props} />
        )}
      </>
    )
  }
}

export const getServerSideProps = wrapper.getServerSideProps(
  async ({store: {dispatch, getState}, query, query: {user}}) => {
    const actions = bindActionCreators({clearPlatforms, getDreams}, dispatch)

    await actions.clearPlatforms()
    const limit = 15

    const args = {
      skip: query.skip || 0,
      limit: limit,
    }

    const dreams = await actions.getDreams({user, args, following: false})

    return {
      props: {
        dreams: dreams.data,
        count: dreams.meta.count || null,
      },
    }
  },
)
