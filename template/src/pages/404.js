import React from 'react'
import Markdown from 'react-markdown'

const message = `
# 404 Error

We couldn't find that.
`

const Lost = () => <Markdown source={message} />

export default Lost
