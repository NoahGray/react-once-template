/* 
  This is trying to answer the Kubernetes healthcheck
*/
import React from 'react'

const Healthz = () => {
  return <>200</>
}

export default Healthz
