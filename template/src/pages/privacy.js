import React from 'react'
import Markdown from 'react-markdown'

const policy = `
# Privacy and safety

We do everything we can to avoid using outside services that might collect your data. The platform has been built with user safety and privacy in mind from the start.

## Error reporting

We host our own error reporting software and configure it to not sure your account credentials or IP address. We do store your browser version and other info to help us understand and fix errors.

## Analytics

We do not use any third-party analytics service and host our own analytics platform. This means your data never goes to Google Analytics or other services. Our analytics data also does not store your full IP address.

## Database

Currently we run our own database and do not use an outside managed database. This may change in the future as costs and complication add up. See below for what data we store.

## Data

Aside from anonymized error reports and analytics which we don't share with outside parties, we store very limited user data in our databse. This data is: email address, display name, account name, date of registration and an encrypted and salted password. We don't sell this data.

The data is outside the jurisdiction of U.S. law and domestic surveillance.

## Payment information

We do not store any payment information. Only the payment service provider you choose when subscribing handles this data. Those parties include Patreon, PayPal, Apple Pay, AliPay, WeChat Pay, and possibly others in the future.

## Hosting

Currently the above services (web, api, database, analytics and error monitoring) are run on cloud-based servers operated by Google. Animations and avatars are store in static store provided by Google as well. We will continue to explore secure and affordable ways to store our services and provide the best availability around the world.
`

const Privacy = () => <Markdown source={policy} />

export default Privacy
