// @flow

import React from 'react'
import Layout from 'components/AppLayout'
import {useRouting} from 'expo-next-react-navigation'
import wrapper from 'store'
// import birdFacts from 'react-twitter-conversion-tracker'

// import {init} from '@socialgouv/matomo-next'

import 'public/style.css'
import 'public/fonts/icons/style.css'

// import {
//   NEXT_PUBLIC_MATOMO_URL,
//   NEXT_PUBLIC_MATOMO_SITE_ID,
//   BIRD_ID,
// } from 'react-native-dotenv'

type Props = {
  Component: any,
  pageProps: {user?: any},
}

const App = (props) => {
  const router = useRouting()

  //   for matomo tracking and twitter ad tracking
  //   componentDidMount() {
  //     init({url: NEXT_PUBLIC_MATOMO_URL, siteId: NEXT_PUBLIC_MATOMO_SITE_ID})
  //     birdFacts.init(BIRD_ID)
  //   }
  //
  //   componentDidUpdate() {
  //     birdFacts.pageView()
  //   }

  const {Component, pageProps} = props

  const {pathname} = router

  if (['/privacy', '/healthz', '/404'].includes(pathname)) {
    return <Component {...pageProps} />
  }

  return (
    <Layout {...pageProps} router={router}>
      <Component {...pageProps} router={router} />
    </Layout>
  )
}

export default wrapper.withRedux(App)
