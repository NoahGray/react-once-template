// @flow

import React from 'react'
import {bindActionCreators} from 'redux'
import {useRouting} from 'expo-next-react-navigation'

import wrapper from 'store'
import Feed from 'components/Feed'
// import Window from 'components/common/Window';

import {getPlatforms, clearPlatforms} from 'store/modules/platforms'

type Props = {
  dreams: Array,
  loaded: boolean,
  params: Object,
  user?: Object,
  router: Object,
}

const Index = (props: Props) => {
  const router = useRouting()

  return <Feed {...props} router={router} />
}

export const getServerSideProps = wrapper.getServerSideProps(
  async ({store, store: {dispatch}}) => {
    const actions = bindActionCreators({clearPlatforms, getPlatforms}, dispatch)

    await actions.clearPlatforms()

    const result = await actions.getPlatforms()

    console.log('result', result)

    return {
      props: {
        platforms: result,
        loaded: true,
      },
    }
  },
)

export default Index
