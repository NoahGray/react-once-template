/* Erik Rasumssen */

import superagent from 'superagent'
import AsyncStorage from '@callstack/async-storage'

import {API_HOST} from 'react-native-dotenv'

export function clientMiddleware(client) {
  return ({dispatch, getState}) => {
    return (next) => (action) => {
      if (typeof action === 'function') {
        return action(dispatch, getState)
      }

      const {promise, types, ...rest} = action // eslint-disable-line no-redeclare
      if (!promise) {
        return next(action)
      }

      const [REQUEST, SUCCESS, FAILURE] = types
      next({...rest, type: REQUEST})

      const actionPromise = promise(client)
      actionPromise
        .then(
          (result) => next({...rest, result, type: SUCCESS}),
          (error) => next({...rest, error, type: FAILURE}),
        )
        .catch((error) => {
          next({...rest, error, type: FAILURE})
          console.error('MIDDLEWARE ERROR', error)
          return
        })

      return actionPromise
    }
  }
}

export class ApiClient {
  methods = ['get', 'post', 'put', 'patch', 'del']

  constructor(req) {
    this.methods.forEach(
      (method) =>
        (this[method] = (path, {query, data, headers} = {}) =>
          new Promise((resolve, reject) => {
            const url = this.formatUrl(path)
            console.log('url', url)
            const request = superagent[method](url)

            AsyncStorage.getItem('authToken').then((authToken) => {
              if (query) {
                request.query(query)
              }

              if (authToken) {
                request.set({accessToken: authToken})
                request.set({TIXTOKEN: authToken})
                request.set({businessType: 'event'})
              }

              if (headers) {
                request.set(headers)
              }

              if (data) {
                request.send(data)
              }

              request.end((err, {body} = {}) =>
                err ? reject(body || err) : resolve(body),
              )
            })
          })),
    )
  }

  formatUrl(path) {
    const adjustedPath = path[0] !== '/' ? '/' + path : path
    return API_HOST + adjustedPath
  }
}
