export function generateConstants(action, path) {
  const base = `redux/${path ? `${path}/` : '/'}${action}`

  return [base, `${base}_SUCCESS`, `${base}_FAILURE`]
}

export const matomo = (store) => (next) => (action) => {
  const actionType = action.type.replace('redux/', '')

  const blacklist = [
    '__NEXT_REDUX_WRAPPER_HYDRATE__',
    'frame/INITIALIZE',
    'saves/GET_SAVES',
    'frame/CHANGE',
    'frame/MOUSE_DOWN',
    'frames/UPDATE_FRAME',
    'frame/SET_BRUSH_MODE',
  ]

  if (
    !blacklist.includes(actionType) &&
    !actionType.endsWith('_SUCCESS') &&
    typeof window !== 'undefined' &&
    !!window._paq
  ) {
    const push = require('@socialgouv/matomo-next').push
    push(['trackEvent', ...actionType.split('/')])
  }

  return next(action)
}

export const palettesList = {
  lomo: [221, 25, 13],
  autumn: [15, 208, 13],
  gameboy: [194, 30, 13],
  peach: [224, 208, 13],
}
