import {createStore, applyMiddleware} from 'redux'
// import remoteDevTools from 'remote-redux-devtools'
import {createWrapper} from 'next-redux-wrapper'
import {ApiClient, clientMiddleware} from './ApiClient'
import reducer from './modules/reducer'
// import {matomo as matomoMiddleware} from './helpers'

// import Raven from 'raven-js'
// import createRavenMiddleware from 'raven-for-redux'

const client = new ApiClient()

if (typeof window !== 'undefined' && process.env.NODE_ENV === 'production') {
  // Raven.config(process.env.SENTRY_DSN, {
  //   dataCallback: (data) => {
  //     data.breadcrumbs.values.reverse()
  //     data.breadcrumbs.values = data.breadcrumbs.values.slice(0, 10)
  //     data.breadcrumbs.values.reverse()
  //     return data
  //   },
  // }).install()
  // middlewares.push(
  //   createRavenMiddleware(Raven, {
  //     stateTransformer: (state) => {
  //       const data = JSON.parse(JSON.stringify(state))
  //       delete data.auth
  //       data.saves.data = data.saves.data && data.saves.data.slice(0, 1)
  //       data.frames.data.frames =
  //         data.frames.data.frames.length > 2
  //           ? data.frames.data.frames.splice(0, 2)
  //           : data.frames.data.frames
  //       data.dreams.data = data.dreams.data.slice(0, 2)
  //       return data
  //     },
  //     getUserContext: (state) => state.auth,
  //   }),
  // )
}

const storeCreator = applyMiddleware(clientMiddleware(client))(createStore)

const makeStore = (context) => storeCreator(reducer)

const wrapper = createWrapper(makeStore)

export default wrapper
