// @flow

import {HYDRATE} from 'next-redux-wrapper'

const GET_PLATFORMS = 'redux/platforms/GET_PLATFORMS'
const GET_PLATFORMS_SUCCESS = 'redux/platforms/GET_PLATFORMS_SUCCESS'
const GET_PLATFORMS_FAILURE = 'redux/platforms/GET_PLATFORMS_FAILURE'
const CLEAR_PLATFORMS = 'redux/platforms/CLEAR_PLATFORMS'
const CLEAR_PLATFORMS_SUCCESS = 'redux/platforms/CLEAR_PLATFORMS_SUCCESS'
const CLEAR_PLATFORMS_FAILURE = 'redux/platforms/CLEAR_PLATFORMS_FAILURE'

const initialState = {
  data: [],
  count: 0,
  loaded: false,
}

// const diffpatch = require("jsondiffpatch").create({
//   objectHash: (obj) => obj._key
// })

export default function reducer(state = initialState, action = {}) {
  switch (action.type) {
    case HYDRATE: {
      return {
        ...action.payload.platforms,
      }
    }

    case CLEAR_PLATFORMS_SUCCESS:
      return {
        data: [],
        count: 0,
        loaded: false,
      }

    case GET_PLATFORMS:
      return state

    case GET_PLATFORMS_SUCCESS: {
      // const newData = [...state.data, ...action.result.platforms]
      // const diff = diffpatch.diff(action.result.data, newData)
      // const testo = diffpatch.patch(newData, diff)

      return {
        ...state,
        data: [...state.data, ...action.result],
        loaded: true,
      }
    }

    case GET_PLATFORMS_FAILURE:
      return state

    default:
      return state
  }
}

export function getPlatforms(
  {user, query, following} = {},
  platforms,
): {
  user?: Object,
  query: Object,
  following?: Boolean,
} {
  const endpoint = user
    ? `/user/${user}/${following ? 'following/platform' : 'platform'}`
    : '/platform'

  return {
    types: [GET_PLATFORMS, GET_PLATFORMS_SUCCESS, GET_PLATFORMS_FAILURE],
    promise: async (client) => {
      if (platforms) {
        const response = await client.get(endpoint, {query})

        return {
          ...response,
          platforms,
        }
      }

      return client.get(endpoint, {
        query,
      })
    },
  }
}

export function clearPlatforms() {
  return {
    types: [CLEAR_PLATFORMS, CLEAR_PLATFORMS_SUCCESS, CLEAR_PLATFORMS_FAILURE],
    promise: () => Promise.resolve(),
  }
}
