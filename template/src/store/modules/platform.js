const GET_DREAM = 'redux/dream/GET_DREAM'
const GET_DREAM_SUCCESS = 'redux/dream/GET_DREAM_SUCCESS'
const GET_DREAM_FAILURE = 'redux/dream/GET_DREAM_FAILURE'
const CLEAR_DREAM = 'redux/dream/CLEAR_DREAM'
const CLEAR_DREAM_SUCCESS = 'redux/dream/CLEAR_DREAM_SUCCESS'
const CLEAR_DREAM_FAILURE = 'redux/dream/CLEAR_DREAM_FAILURE'

const initialState = {
  data: null,
  loaded: false
}

export default function reducer (state = initialState, action = {}) {
  switch (action.type) {
    case CLEAR_DREAM_SUCCESS:
      return initialState

    case GET_DREAM:
      return initialState

    case GET_DREAM_SUCCESS:
      return {
        ...state,
        ...action.result,
        loaded: true
      }

    case GET_DREAM_FAILURE:
      return initialState;

    default:
      return state;
  }
}

export function getDream ({ user, dream } = {}) {
  const endpoint = `/user/${user}/dream/${dream}`

  return {
    types: [GET_DREAM, GET_DREAM_SUCCESS, GET_DREAM_FAILURE],
    promise: (client) => client.get(endpoint)
  }
}

export function clearDream () {
  return {
    types: [CLEAR_DREAM, CLEAR_DREAM_SUCCESS, CLEAR_DREAM_FAILURE],
    promise: () => Promise.resolve()
  }
}
