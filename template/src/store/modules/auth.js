import AsyncStorage from '@callstack/async-storage'

const LOGIN = 'redux-example/auth/LOGIN'
const LOGIN_SUCCESS = 'redux-example/auth/LOGIN_SUCCESS'
const LOGIN_FAIL = 'redux-example/auth/LOGIN_FAIL'
const REGISTER = 'redux/auth/REGISTER'
const REGISTER_SUCCESS = 'redux/auth/REGISTER_SUCCESS'
const REGISTER_FAILURE = 'redux/auth/REGISTER_FAILURE'
const LOGOUT = 'redux/auth/LOGOUT'
const LOGOUT_SUCCESS = 'redux/auth/LOGOUT_SUCCESS'
const LOGOUT_FAILURE = 'redux/auth/LOGOUT_FAILURE'
const GET_ME = 'redux/auth/GET_ME'
const GET_ME_SUCCESS = 'redux/auth/GET_ME_SUCCESS'
const GET_ME_FAILURE = 'redux/auth/GET_ME_FAILURE'
const UPDATE = 'redux/auth/UPDATE'
const UPDATE_SUCCESS = 'redux/auth/UPDATE_SUCCESS'
const UPDATE_FAILURE = 'redux/auth/UPDATE_FAILURE'
const RESET = 'redux/auth/RESET'
const RESET_SUCCESS = 'redux/auth/RESET_SUCCESS'
const RESET_FAILURE = 'redux/auth/RESET_FAILURE'
const RESET_POST = 'redux/auth/RESET_POST'
const RESET_POST_SUCCESS = 'redux/auth/RESET_POST_SUCCESS'
const RESET_POST_FAILURE = 'redux/auth/RESET_POST_FAILURE'

const initialState = {
  loaded: false,
}

export default function reducer(state = initialState, action = {}) {
  switch (action.type) {
    case LOGIN:
      return {
        ...state,
        loaded: false,
      }
    case LOGIN_SUCCESS:
      AsyncStorage.setItem('authToken', action.result.data.authToken)

      return {
        ...state,
        ...action.result,
        loaded: true,
      }
    case LOGIN_FAIL:
      return {
        ...initialState,
      }
    case GET_ME_SUCCESS:
      return {
        ...state,
        ...action.result,
        loaded: true,
      }
    case REGISTER_SUCCESS:
      AsyncStorage.setItem('authToken', action.result.data.authToken)

      return {
        ...state,
        ...action.result,
        loaded: true,
      }
    case UPDATE:
      return {
        ...state,
        loaded: false,
      }
    case UPDATE_SUCCESS:
      return {
        ...state,
        ...action.result,
        loaded: true,
      }
    case RESET:
      return {
        ...state,
        loaded: false,
      }
    case RESET_SUCCESS:
      return {
        ...state,
        loaded: true,
      }
    case LOGOUT:
      AsyncStorage.removeItem('authToken')

      return {
        ...initialState,
      }

    default:
      return state
  }
}

export function isLoaded(globalState) {
  return globalState.auth && globalState.auth.loaded
}

export function login(credentials) {
  return {
    types: [LOGIN, LOGIN_SUCCESS, LOGIN_FAIL],
    promise: (client) =>
      client.post('/session', {
        data: credentials,
      }),
  }
}

export function passwordReset(email) {
  return {
    types: [RESET, RESET_SUCCESS, RESET_FAILURE],
    promise: (client) =>
      client.get('/password', {
        query: {email},
      }),
  }
}

export function postPasswordReset({resetToken, password}) {
  return {
    types: [RESET_POST, RESET_POST_SUCCESS, RESET_POST_FAILURE],
    promise: (client) =>
      client.post('/password', {
        data: {resetToken, password},
      }),
  }
}

export function register(credentials) {
  return {
    types: [REGISTER, REGISTER_SUCCESS, REGISTER_FAILURE],
    promise: (client) =>
      client.post('/user', {
        data: credentials,
      }),
  }
}

export function updateUser({name, fields}) {
  return {
    types: [UPDATE, UPDATE_SUCCESS, UPDATE_FAILURE],
    promise: (client) =>
      client.patch(`/user/${name}`, {
        data: fields,
      }),
  }
}

export function getMe() {
  return {
    types: [GET_ME, GET_ME_SUCCESS, GET_ME_FAILURE],
    promise: async (client) =>
      client.get('/session', {
        headers: {
          Authorization: await AsyncStorage.getItem('authToken'),
        },
      }),
  }
}

export function logout() {
  return {
    types: [LOGOUT, LOGOUT_SUCCESS, LOGOUT_FAILURE],
    promise: () => Promise.resolve(),
  }
}
