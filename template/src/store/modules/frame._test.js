import test from 'tape'
import { swapColor, palette, floodColor } from './frame'
import mockCircle from '../../mock-data/frame-with-circle'
import mockCircleFilled from '../../mock-data/frame-with-circle-filled'

test('Invert individual colors', (t) => {
  const paletteColor1 = swapColor(palette[0])
  const paletteColor0 = swapColor(palette[1])

  t.equals(paletteColor1, palette[1], 'Foreground becomes background')
  t.equals(paletteColor0, palette[0], 'Background becomes foreground')
  t.end()
})

test('Use flood tool', (t) => {
  const state = {
    data: mockCircle
  }
  const action = { result: { drop: [10, 20] } }
  const flooded = floodColor(state, action)

  t.equals(JSON.stringify(flooded), JSON.stringify(mockCircleFilled), 'Flood tool fills circle')
  t.end()
})
