import {combineReducers} from 'redux'
import {HYDRATE} from 'next-redux-wrapper'

import auth from './auth'
import platform from './platform'
import platforms from './platforms'

const nextWrapperReducer = (state = {}, action) => {
  switch (action.type) {
    case HYDRATE: {
      return {...state, ...action.payload}
    }

    case 'TICK':
      return {...state, tick: action.payload}

    default:
      return state
  }
}

export default combineReducers({
  nextWrapperReducer,
  auth,
  platform,
  platforms,
})
