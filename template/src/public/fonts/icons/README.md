# Using Icons

Custom icon sets can be generated with https://icomoon.io/app. Export them as a font and the zip file will contain files like the ones in this folder. Then, see `src/components/common/Icons.js` for how to create the `Icon` component.

You will also need to load the fonts on mobile in App.js as I have done with `expo-fonts`. For web, we must use the .css file which we can import in `src/pages/_app.js`.

You should also update the path in the `style.css` to include a subfolder if you put your icons in one as I have.
