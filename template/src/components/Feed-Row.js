// @flow

import React, {useState} from 'react'
import styled from 'styled-components/native'
import {Text} from 'react-native'
import {Link} from 'expo-next-react-navigation'

import {Column, Row} from './common/Flexbox'

import {NEXT_PUBLIC_STORAGE_ENDPOINT, OBJ_STORE_URL} from 'react-native-dotenv'

type Props = {
  key: any,
  style?: Object,
  dream: Object,
  url?: Object,
}

const Container = styled.View`
  display: flex;
  flex-direction: column;
  padding: 9px 12px;
  background-color: #2d2d2d;
  color: white;
  align-items: center;
`

const ProfilePicture = styled.Image`
  height: 48px;
  min-width: 48px;
  margin-right: 12px;
  border-radius: 24px;
`

// background-image: ${({imageUrl}) => (imageUrl ? `url(${imageUrl})` : 'grey')};
// background-size: cover;
// background-position: center;

const DisplayName = styled.Text`
  font-size: 16px;
  color: white;
`

// font-family: 'jackeyfont';

const Dream = styled.Image`
  height: 256px;
  width: 384px;
`

const RowRenderer = ({dream, style, url}: Props) => {
  const [loaded, setLoaded] = useState(false)
  /* We want relative image paths without forward slashes. This is becuase our data storage engine doesn't like slashes in the keys. So we store them as string keys. Some old values may have forward slashes, so we remove them. */
  let avatarUrl = dream.user.avatar || ''
  avatarUrl = avatarUrl.includes('http')
    ? dream.user.avatar
    : `${NEXT_PUBLIC_STORAGE_ENDPOINT}/${dream.user.avatar}`

  const imageUri = `${dream.urls.x8.replace(
    OBJ_STORE_URL,
    NEXT_PUBLIC_STORAGE_ENDPOINT,
  )}`

  return (
    <Container style={style}>
      <Column style={{maxWidth: 384}}>
        <Row>
          <ProfilePicture imageUrl={{uri: avatarUrl}} />
          <Column
            style={{
              color: '#437ABC',
              justifyContent: 'space-between',
              height: 40,
            }}
          >
            <DisplayName>{dream.user.displayName}</DisplayName>
            <Link
              routeName="/user"
              params={{user: dream.user.name}}
              web={{
                path: '/user/[user]',
                as: `/user/${dream.user.name}`,
              }}
              scroll={false}
            >
              <Text style={{color: '#CD351E'}}>@{dream.user.name}</Text>
            </Link>
          </Column>
        </Row>
        <Column style={{alignItems: 'center'}}>
          <Dream source={{uri: imageUri}} />
        </Column>
      </Column>
    </Container>
  )
}

export default RowRenderer
