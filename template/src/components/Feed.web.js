// @flow

import React, {Component} from 'react'
import {bindActionCreators} from 'redux'
import {
  SafeAreaView,
  View,
  VirtualizedList,
  StyleSheet,
  Text,
} from 'react-native'
import {autobind} from 'core-decorators'
import {connect} from 'react-redux'

import {isEmpty} from 'lodash'
import styled from 'styled-components/native'

import FeedRow from 'components/Feed-Row'

import {getPlatforms, clearPlatforms} from 'store/modules/platforms'
import {getDream} from 'store/modules/platform'

type Props = {
  location: Object,
  platforms: Array,
  dream: Object,
  loaded: boolean,
  getPlatforms: Function,
  clearPlatforms: Function,
  count: number,
  params: Object,
  user?: Object,
  url: Object,
}

const ScrollerContainer = styled.View`
  max-width: 384px;
  display: flex;
  flex-direction: row;
`

const FeedContainer = styled.SafeAreaView`
  flex: 2;
`

@connect(
  (state) => ({
    platforms: state.platforms.data,
  }),
  (dispatch) =>
    bindActionCreators(
      {
        getPlatforms,
        clearPlatforms,
      },
      dispatch,
    ),
)
export default class Feed extends Component<Props> {
  @autobind
  isRowLoaded({index}) {
    return !!this.props.platforms[index]
  }

  @autobind
  rowLoadHandler(data, index) {
    const {count, platforms} = this.props
    // console.log('requesting dream', index, 'of', this.props.platforms.length)
    if (index < platforms.length - 1) {
      const dream = platforms[index]

      return dream
        ? {
            ...dream,
            key: dream._key,
          }
        : undefined
    }

    if (index >= count - 1) {
      const dream = platforms[index]

      return dream
        ? {
            ...dream,
            key: dream._key,
          }
        : undefined
    }

    const {params, pathname} = this.props.router
    const currentUser = params && params.user
    const isFollowingFeed = pathname.endsWith('/following')

    const args = {
      skip: index + 1,
      limit: 4,
    }

    return this.props
      .getPlatforms({
        query: args,
        user: currentUser || undefined,
        following: isFollowingFeed,
      })
      .then((data) => {
        console.log('got platforms!', this.props.platforms.length, data)
        const dream = platforms[index]

        return dream
          ? {
              ...dream,
              key: dream._key,
            }
          : undefined
      })
  }

  render() {
    const {
      router,
      router: {asPath, query: params},
      loaded,
      platforms,
      user,
    } = this.props

    /* Try make the count (and thus page length) higher than the current number of items, but never higher than what's actually in the DB. */
    let count = platforms.length + 15
    if (this.props.count - count <= 10) {
      count = this.props.count
    }

    const RowRenderer = (...args) => {
      const {index, key, style} = args[0]
      const dream = platforms[index]

      if (this.isRowLoaded({index})) {
        return (
          <FeedRow
            dream={dream}
            key={key}
            style={style}
            url={this.props.router}
          />
        )
      }

      return null
    }

    if (!platforms) return null

    return (
      <FeedContainer>
        <VirtualizedList
          data={platforms}
          initialNumberToRender={5}
          renderItem={RowRenderer}
          keyExtractor={(data, index) => {
            if (data && data.length > 0) {
              return data[index]._key
            }
          }}
          getItemCount={() => 4}
          getItem={this.rowLoadHandler}
        />
      </FeedContainer>
    )
  }
}
