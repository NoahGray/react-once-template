// @flow

import React, {Component} from 'react'

import {connect} from 'react-redux'
import {bindActionCreators} from 'redux'
import styled from 'styled-components/native'

import {getMe, logout} from 'store/modules/auth'
import {clearPlatforms} from 'store/modules/platforms'
import AsyncStorage from '@callstack/async-storage'

// import TabBar from 'components/TabBar'
// import Window from 'components/common/Window'
// import Login from 'components/common/Login'
// import Profile from 'components/Profile'
// import ErrorBoundry from 'components/ErrorBoundry'
import Header from 'components/Header'

type Props = {
  url: Object,
  user: Object,
  loaded: boolean,
  getMe: Function,
  clear: Function,
  logout: Function,
  children: React$Element,
  router: Object,
}

const Container = styled.SafeAreaView`
  flex: 1;
  background-color: #2d292d;
`

const Heading = styled.Text`
  padding: 0;
  margin: 0;
  color: #4079be;
  line-height: 0;
  height: 100%;
  display: flex;
  align-items: center;
`

const ScrollView = styled.ScrollView`
  flex: 2;
  background-color: #2d292d;
`

@connect(
  (state) => ({
    user: state.auth.data,
    loaded: state.auth.loaded,
  }),
  (dispatch) =>
    bindActionCreators(
      {
        getMe,
        logout,
        clear,
      },
      dispatch,
    ),
)
class Layout extends Component<Props> {
  async componentDidMount() {
    if ((await AsyncStorage.getItem('authToken')) && !this.props.loaded) {
      this.props.getMe()
    }
  }

  render() {
    const {children, router, user} = this.props

    // Can't find a way to just dismiss the modal, which is URL dependant. Annoying as hell.
    const modalDismiss = user
      ? [
          {pathname: '/user/[user]', query: {user: user.name}},
          `/user/${user.name}`,
          {shallow: false},
        ]
      : [{pathname: router.pathname === '/editor' ? '/editor' : '/'}]

    return (
      <Container>
        <Header router={router} />
        {children &&
          React.cloneElement(children, {
            user: this.props.user,
          })}
      </Container>
    )
  }
}

const AppLayout = (props) => {
  const router = {
    ...props.navigation,
    query: props.navigation.params || {},
  }

  return <Layout {...props} router={router} />
}

// <TabBar
// user={this.props.user}
// getMe={getMe}
// clear={clear}
// initialize={initialize}
// logout={logout}
// />

export default AppLayout
