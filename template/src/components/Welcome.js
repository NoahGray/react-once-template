import React from 'react'
import {Link} from 'expo-next-react-navigation'
import {Text} from 'react-native'
import styled from 'styled-components/native'

import {NEXT_STATIC_STORAGE_ENDPOINT} from 'react-native-dotenv'

const welcome = `${NEXT_STATIC_STORAGE_ENDPOINT}/welcome.gif`

const Container = styled.SafeAreaView`
  flex: 2;
  align-items: center;
  justify-content: center;
  flex-direction: row;
`

const Dialog = styled.Text`
  align-items: center;
  font-family: manaspace;
  color: white;
`

const LinksContainer = styled.View`
  flex: 1;
`

const WelcomerImage = styled.Image`
  height: 256px;
  width: 384px;
`

const Welcome = (props) => {
  return (
    <Container>
      <WelcomerImage source={{uri: welcome}} />
      {props.isOwnPage ? (
        <Dialog>
          Welcome. This is your feed. It's emptry, so try making something with
          the editor.
        </Dialog>
      ) : (
        <Dialog>There is nothing to see here yet.</Dialog>
      )}
      <LinksContainer>
        <Link routeName="/editor" web={{path: '/editor'}} shallow>
          <Text>Go to editor</Text>
        </Link>
        {!props.isOwnPage && (
          <Link routeName="Home" web={{path: '/'}}>
            <Text>Or meet people to follow</Text>
          </Link>
        )}
      </LinksContainer>
    </Container>
  )
}

export default Welcome
