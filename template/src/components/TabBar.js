// @flow

import React, {Component} from 'react'
import styled from 'styled-components/native'

import Navbar from 'components/common/Navbar'

import * as Item from 'components/Header-Item'

type Props = {
  url: Object,
  user: Object,
  clear: Function,
  children: React$Element,
}

const MenuBar = styled.View`
  background: red;
  justify-content: space-evenly;
  position: absolute;
  bottom: 0px;
  left: 0px;
  right: 0px;
`

export default class TabBar extends Component<Props> {
  state = {
    showPaletteModal: false,
    isDismissed: false,
  }

  // Only show the modal if they're signed in and haven't
  // dismissed it already
  openEditorHandler = () => {
    const {router, user} = this.props
    const {showPaletteModal, isDismissed} = this.state

    router.push({routeName: '/editor'})

    if (user && !isDismissed) {
      this.setState({showPaletteModal: true})
    }
  }

  dismissModalHandler = () => {
    const {showPaletteModal, isDismissed} = this.state

    this.setState({showPaletteModal: false, isDismissed: true})
  }

  render() {
    const {
      router,
      // router: {query},
      user,
    } = this.props

    // const isFollowingFeed = router.pathname.endsWith('/following')

    return (
      <Navbar>
        <Item.Platforms style={{flexDirection: 'column'}} router={router} />
        <Item.Gitlab router={router} />
        <Item.Home router={router} />
      </Navbar>
    )
  }
}

// {query.user && !isFollowingFeed && <UserSummary query={query} />}

// <style jsx>{
//   /* syntax: css */ `
//     @media (max-width: 414px) {
//       h1 {
//         display: none;
//       }
//
//       div :global(.updates > span) {
//         display: none;
//       }
//     }
//   `
// }</style>
// <style jsx global>{
//   /* syntax: css */ `
//     body {
//       padding: 0;
//       margin: 0;
//       font-family: -apple-system-font, "Helvetica Neue", Helvetica,
//         lotus;
//       background-color: #1a1a1a;
//     }
//
//     @media (max-width: 414px) {
//       .hideOnMobile {
//         display: none !important;
//       }
//     }
//   `
// }</style>
