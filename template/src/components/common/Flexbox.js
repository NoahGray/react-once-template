// @flow

import React from "react"
import styled from "styled-components/native"

type Props = {
  children: React$Element,
  className?: string,
  style?: object,
  onClick?: func,
}

const RowContainer = styled.View`
  display: flex;
  flex-direction: row;
`

const ColumnContainer = styled.View`
  display: flex;
  flex-direction: column;
`

const Row = ({ children, className, style }: Props) => {
  return (
    <RowContainer style={style} className={className}>
      {children}
    </RowContainer>
  )
}

const Column = ({ onClick, children, className, style }: Props) => {
  return (
    <ColumnContainer style={style} className={className} onClick={onClick}>
      {children}
    </ColumnContainer>
  )
}

export { Row, Column }
