// @flow

import React from 'react'
import print from 'components/labels'

type ButtonProps = {
  colors: Array<string>,
  children: any,
  onClick?: Function,
  centered?: boolean,
}

const PixelGradientText = (props: ButtonProps) => {
  const {colors, children, onClick} = props
  const modifier = 100 / colors.length

  return (
    <div
      onClick={onClick ? onClick : undefined}
      style={{
        backgroundImage: `linear-gradient(to bottom, 
          ${colors[0]} 0%, ${colors[0]} ${modifier * 1}%,
          ${colors[1]} ${modifier * 1}%, ${colors[1]} ${modifier * 2}%,
          ${colors[2]} ${modifier * 2}%, ${colors[2]} ${modifier * 3}%,
          ${colors[3]} ${modifier * 3}%, ${colors[3]} ${modifier * 4}%,
          ${colors[4]} ${modifier * 4}%, ${colors[4]} ${modifier * 5}%,
          ${colors[5]} ${modifier * 5}%, ${colors[5]} ${modifier * 6}%,
          ${colors[6]} ${modifier * 6}%, ${colors[6]} 100%
        )`,
      }}
    >
      <style jsx>{`
        div {
          font-family: lotus;
          margin-top: 15px;
          font-size: 35px;
          line-height: 27px;
          cursor: pointer;
          color: transparent;
          -webkit-background-clip: text;
          background-clip: text;
          ${props.centered
            ? `
            text-align: center;
          `
            : ''}
        }

        @media (max-width: 414px) {
          div {
            font-size: 40px;
          }
        }

        @media (max-width: 320px) {
          div {
            font-size: 30px;
          }
        }
      `}</style>
      {print(children)}
    </div>
  )
}

export default PixelGradientText
