// @flow

import React, {Component} from 'react'
import {View, Text} from 'react-native'
import {Form, Field} from 'react-final-form'
import {FORM_ERROR} from 'final-form'
import {bindActionCreators} from 'redux'
import {connect} from 'react-redux'
import {autobind} from 'core-decorators'
import {isAlphanumeric, isLength, isEmail, trim, isNumeric} from 'validator'
import styled from 'styled-components/native'
import {TouchableOpacity} from 'react-native'

import {
  postPasswordReset,
  passwordReset,
  register,
  login,
} from 'store/modules/auth'

import {Column, Row} from 'common/Flexbox'
import Register from './Register'
import PixelGradientText from './PixelGradientText'
import {TextField as Input} from '../Fields'
import print from 'components/labels'

type ModeProps = {
  registering?: boolean,
  registerClickHandler?: Function,
}

const TextField = styled(Input)`
  background-color: #3c3a3c;
  width: 100%;
  color: white;
  padding: 10px;
  font-family: lotus;
  font-size: 20;
`

const ModeButtonText = styled.Text`
  color: white;
  font-size: 20;
  justify-content: space-between;
  font-family: lotus;
  margin-top: 20px;
  margin-bottom: 10px;
`

const ModeContainer = styled.View`
  justify-content: space-around;
  flex-direction: row;
  margin-bottom: 20px;
`

const colors = [
  '#2323f4',
  '#4923f4',
  '#6823f4',
  '#8F24F4',
  '#AF25F4',
  '#D626F5',
  '#F627F5',
]

const ModeButtons = ({registering, registerClickHandler}: ModeProps) => (
  <ModeContainer>
    <TouchableOpacity onPress={registerClickHandler}>
      <ModeButtonText>
        {!registering ? `> ${print('Login')}` : ` ${print('Login')}`}
      </ModeButtonText>
    </TouchableOpacity>
    <TouchableOpacity onPress={registerClickHandler}>
      <ModeButtonText>
        {registering ? `> ${print('Register')}` : ` ${print('Register')}`}
      </ModeButtonText>
    </TouchableOpacity>
  </ModeContainer>
)

type Props = {
  register: func,
  login: func,
  onSubmit: func,
  fields: object,
  handleSubmit: func,
  onToggleRegistering: func,
  registering: boolean,
  hideLogin: boolean,
  location: object,
  dismiss: func,
  reset?: boolean,
  postPasswordReset: Function,
  passwordReset: Function,
}

export default
@connect(
  (state) => ({
    user: state.auth.user,
  }),
  (dispatch) =>
    bindActionCreators(
      {
        login,
        register,
        passwordReset,
        postPasswordReset,
      },
      dispatch,
    ),
)
class Login extends Component<Props> {
  state = {
    showResetTokenForms: false,
  }

  @autobind
  registerClickHandler() {
    const {router} = this.props
    const registering = router && router.getParam('register')

    const params = registering ? {login: true} : {register: true}

    this.props.router.push({
      params,
      web: {
        path: router.pathname,
        shallow: true,
      },
    })
  }

  @autobind
  submitHandler(fields) {
    const {registering, onSubmit, passwordReset} = this.props
    const {reset} = this.props.location.query
    const {showResetTokenForms} = this.state

    if (!!reset && !showResetTokenForms) {
      try {
        passwordReset(fields.email)
      } catch (e) {
        return {[FORM_ERROR]: e}
      }

      this.setState({showResetTokenForms: true})
      return
    }

    if (showResetTokenForms && fields.resetToken && fields.password) {
      try {
        this.props.postPasswordReset({
          resetToken: fields.resetToken,
          password: fields.password,
        })
      } catch (error) {
        return {[FORM_ERROR]: error}
      }

      return this.props.dismiss()
    }

    if (onSubmit) {
      try {
        onSubmit(fields)
      } catch (e) {
        return {[FORM_ERROR]: e}
      }

      return this.props.dismiss ? this.props.dismiss() : this.cancelHandler()
    }

    if (registering) {
      return this.props
        .register({
          // eslint-disable-line consistent-return
          ...fields,
        })
        .then(() => {
          this.props.dismiss()
        })
        .catch((error) => {
          return {[FORM_ERROR]: error}
        })
    }

    return this.props
      .login(fields)
      .then(() => {
        this.props.dismiss()
      })
      .catch((error) => {
        return {[FORM_ERROR]: error}
      })
  }

  /* Presently this is almost identical to server-side. Perhaps in future we could use lerna(?) to keep two validation libraries in sync on front and back. */
  userValidator(values) {
    const {registering} = this.props
    const reset = this.props.router.getParam('reset')

    if (!values.email) {
      return {
        email: print('Please provide email'),
      }
    }

    if (reset) {
      return
    }

    if (!values.password) {
      return {
        password:
          'The rule is everyone needs a password to get in, sorry. Fun part is you make up your own!',
      }
    }

    if (!registering || Object.keys(values).length === 0) {
      return
    }

    const user = {
      email: trim(values.email).toLowerCase(),
      password: trim(values.password),
      name: !registering ? trim(values.name).toLowerCase() : undefined,
      displayName: !registering
        ? trim(values.displayName).toLowerCase()
        : undefined,
      ...values,
    }

    if (registering) {
      if (!user.name) {
        return {
          name: "Your name is you '@DarkLordX' type name. Everyone needs one.",
        }
      }
      if (!isAlphanumeric(user.name)) {
        return {
          name:
            "Look... I'll let you use numbers (and letters), but no punctuation, OK?",
        }
      }
      if (isNumeric(user.name)) {
        return {
          name:
            "I didn't ask for a serial number, doesn't your name have letters?",
        }
      }
      if (!isLength(user.name, 3, 32)) {
        return {
          name:
            "I don't make the rules, but they want names between 3 and 32 characters here. Sorry.",
        }
      }

      if (!isEmail(user.email)) {
        return {email: "Where I come from emails don't look like that..."}
      }
      if (!isLength(user.email, 5, 255)) {
        return {
          email:
            "I'm used to emails being a different length. You sure this is right?",
        }
      }

      if (!isLength(user.password, 4, 255)) {
        return {
          password:
            'We kinda prefer my passwords between 4 and 255 characters long.',
        }
      }
    }
  }

  @autobind
  passwordResetHandler() {
    const {reset, router} = this.props

    if (!['true', true].includes(reset)) {
      return router.push({path: router.path, params: {reset: true}})
    }

    return
  }

  render() {
    const {reset, router} = this.props
    const login = router.getParam('login')
    const registering = router.getParam('register')
    const {showResetTokenForms} = this.state

    console.log('reg', registering)

    return (
      <Form
        id="Login"
        onSubmit={this.submitHandler}
        validate={(values) => this.userValidator(values)}
        render={({handleSubmit, submitError}) => {
          return (
            <Column style={{flex: 1, width: '100%'}}>
              {!reset && (
                <ModeButtons
                  registering={registering}
                  registerClickHandler={this.registerClickHandler}
                />
              )}
              <Column>
                {registering && (
                  <Register userValidator={this.userValidator.bind(this)} />
                )}
                <Field
                  id="Login_email"
                  name="email"
                  label="Email"
                  type="text"
                  component={TextField}
                />
                {showResetTokenForms && (
                  <Field
                    id="Reset_token"
                    name="resetToken"
                    label="Reset token"
                    type="text"
                    component={TextField}
                  />
                )}
                {(login || registering || showResetTokenForms) && (
                  <Field
                    id="Login_password"
                    name="password"
                    label="Password"
                    type="password"
                    component={TextField}
                  />
                )}
                {submitError && (
                  <View
                    style={{
                      width: '100%',
                      textAlign: 'center',
                      color: 'red',
                      fontSize: 20,
                    }}
                  >
                    <Text>
                      Oops! Something went wrong signing in :( Check your info!
                    </Text>
                  </View>
                )}
              </Column>
              <Row
                style={{
                  display: 'flex',
                  justifyContent: 'space-around',
                }}
              >
                <PixelGradientText
                  onClick={!reset ? this.passwordResetHandler : handleSubmit}
                  className="pixelGradientText"
                  centered
                  colors={colors}
                >
                  {`${reset ? 'Reset password' : 'Reset password'}`}
                </PixelGradientText>
                {!reset && (
                  <PixelGradientText
                    onClick={handleSubmit}
                    className="pixelGradientText"
                    centered
                    colors={colors}
                  >
                    Submit
                  </PixelGradientText>
                )}
              </Row>
            </Column>
          )
        }}
      />
    )
  }
}
