// @flow

import React from 'react'
import styled from 'styled-components/native'

type Props = {
  children: React.Node,
  style?: Object,
}

const Navbar = styled.SafeAreaView`
  background-color: #3c3a3c;
  padding-top: 5px;
  flex-direction: row;
  justify-content: space-around;
`

export default Navbar
