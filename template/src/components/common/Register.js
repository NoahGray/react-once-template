/* @flow */
import React, {Component} from 'react'
import {Field} from 'react-final-form'
import styled from 'styled-components/native'

import {TextField as Input} from '../Fields'

const TextField = styled(Input)`
  background-color: #3c3a3c;
  width: 100%;
  color: white;
  padding: 10px;
  font-family: lotus;
  font-size: 20;
`

type Props = {
  builder?: boolean,
  subcontractor?: boolean,
  children?: React.Element,
  location?: Object,
  register?: Function,
  onKeyUp?: Function,
}

export default class Register extends Component {
  props: Props

  render() {
    return (
      <>
        <Field
          className="reduxField"
          id="new_account_name"
          name="name"
          label="Account name"
          component={TextField}
          type="text"
        />
        <Field
          className="reduxField"
          name="displayName"
          label="Display name"
          component={TextField}
          type="text"
        />
      </>
    )
  }
}
