import {createIconSetFromIcoMoon} from '@expo/vector-icons'

import config from 'public/fonts/icons/selection.json'

const Icon = createIconSetFromIcoMoon(config)

export default Icon
