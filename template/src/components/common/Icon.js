// @flow

import React from 'react'

import Icons from './Icons'

// filter: drop-shadow(2px 2px 0px black);

type Props = {
  icon: string,
  color?: string,
  size?: number,
  isPixelated?: Boolean,
}

const Icon = ({
  icon,
  color,
  size = 24,
  style,
  isPixelated,
  flexDirection = 'column',
}: Props) => {
  return (
    <Icons
      name={icon}
      color={color || '#A0A0A0'}
      size={size}
      style={{flexDirection: flexDirection}}
      flexDirection={flexDirection}
    />
  )
}

export default Icon
