// @flow

import React, {Component} from 'react'
import {StyleSheet, Platform, View, Modal} from 'react-native'
import {autobind} from 'core-decorators'
import styled from 'styled-components/native'
import {Link} from 'expo-next-react-navigation'

import {NEXT_STATIC_STORAGE_ENDPOINT} from 'react-native-dotenv'

import print from 'components/labels'
import Icon from 'common/Icon'

type Props = {
  children: any,
  params?: Object,
  className?: string,
  overlayClassName?: string,
  dismiss?: Array,
  style?: Object,
  as?: string,
  router: Object,
}

const Container = styled.View`
  padding: 50px;
  flex: 1;
`

const BackContainer = styled.View`
  flex-direction: row;
  position: fixed;
  top: 25px;
  left: 25px;
  background-position: left;
  image-rendering: optimize-contrast;
  image-rendering: pixelated;
  background-repeat: no-repeat;
  background-size: 40px 40px;
  width: 130px;
  height: 40px;
  display: flex;
  padding-left: 14px;
  align-items: center;
`

const BackText = styled.Text`
  font-family: lotus;
  font-size: 25px;
  color: white;
  margin-left: 30px;
  width: 300px;
  flex-direction: row;
  display: flex;
`

export default class Window extends Component<Props> {
  render() {
    const {router, children} = this.props
    const origin = router ? router.getParam('origin') : '/'

    const dismiss = this.props.dismiss || [{pathname: router.asPath}]

    return (
      <Modal visible>
        <Container style={{background: 'black'}}>
          <Link
            web={{
              path: origin,
            }}
            shallow
            scroll={false}
          >
            <BackContainer>
              <Icon icon="cross" color="white" />
              <BackText>{print('Back')}</BackText>
            </BackContainer>
          </Link>
          {children}
        </Container>
      </Modal>
    )
  }
}
