// @flow

import React from 'react'
import {View, Text, Pressable} from 'react-native'
import MaskedView from '@react-native-community/masked-view'
import print from 'components/labels'

type ButtonProps = {
  colors: Array<string>,
  children: any,
  onClick?: Function,
  centered?: boolean,
}

const PixelGradientText = (props: ButtonProps) => {
  const {colors, children, onClick, centered} = props
  const modifier = 100 / colors.length

  return (
    <Pressable
      onPress={onClick ? onClick : undefined}
      style={{
        justifyContent: centered ? 'center' : 'flex-start',
        flex: 1,
        marginTop: 15,
      }}
    >
      <MaskedView
        style={{height: 50, width: '100%'}}
        maskElement={
          <Text
            style={{
              fontSize: 60,
              fontFamily: 'jackeyfont',
              textAlign: 'center',
            }}
          >
            {print(children)}
          </Text>
        }
      >
        <View
          style={{backgroundColor: colors[0], height: `${modifier * 1}%`}}
        />
        <View
          style={{backgroundColor: colors[1], height: `${modifier * 1}%`}}
        />
        <View
          style={{backgroundColor: colors[2], height: `${modifier * 1}%`}}
        />
        <View
          style={{backgroundColor: colors[3], height: `${modifier * 1}%`}}
        />
        <View
          style={{backgroundColor: colors[4], height: `${modifier * 1}%`}}
        />
        <View
          style={{backgroundColor: colors[5], height: `${modifier * 1}%`}}
        />
        <View
          style={{backgroundColor: colors[6], height: `${modifier * 1}%`}}
        />
      </MaskedView>
    </Pressable>
  )
}

export default PixelGradientText
