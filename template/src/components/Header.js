// @flow

import React, {Component} from 'react'
import {Text} from 'react-native'
import styled from 'styled-components/native'
import {connect} from 'react-redux'
import {bindActionCreators} from 'redux'
import {useRouting} from 'expo-next-react-navigation'

import {Row} from 'components/common/Flexbox'
import Navbar from 'components/common/Navbar'

import * as Item from 'components/Header-Item'

type Props = {
  url: Object,
  user: Object,
  clear: Function,
  children: React$Element,
}

const TopNav = styled(Navbar)`
  height: 85px;
  align-items: flex-end;
`

const Heading = styled.Text`
  padding: 0;
  color: #4079be;
  line-height: 25px;
  font-size: 2em;
  text-shadow: 1px 1px 0px black;
  text-align: center;
  self-align: center;
  margin: 0 0 6px 0;
`

const Logo = styled.Image`
  height: 61px;
  width: 61px;
`

const LogoContainer = styled.View`
  height: 64px;
  width: 250px;
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
  display: flex;
`

const SiteTitle = styled.Text`
  color: white;
  font-weight: 700;
  font-size: 30;
`

const Header = (props: Props) => {
  const router = useRouting()

  return (
    <TopNav>
      <Item.Twitter router={router} />
      <LogoContainer>
        <Logo source={{uri: '/logo192.png'}} />
        <SiteTitle>ReactOnce</SiteTitle>
      </LogoContainer>
      <Item.Profile router={router} />
      <Item.Logout router={router} />
    </TopNav>
  )
}

export default Header
