/* @flow */
import React, {Component} from 'react'
import * as Sentry from '@sentry/browser'
import {View} from 'react-native'
import styled from 'styled-components/native'

import {SENTRY_DSN} from 'react-native-dotenv'

Sentry.init({
  dsn: SENTRY_DSN,
})
// should have been called before using it here
// ideally before even rendering your react app

const Overlay = styled.View`
  position: fixed;
  top: 0;
  bottom: 0;
  height: 100vh;
  width: 100vw;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  display: flex;
  z-index: 2;
  backdrop-filter: blur(3px);
`

const FeedbackContainer = styled.TouchableOpacity`
  background: white;
  padding: 5px 10px;
`

const FeedbackLabel = styled.Text`
  color: red;
  font-size: 25px;
`

const ErrorLabel = styled.Text`
  color: white;
  font-size: 1.2em;
  background: blue;
  margin-bottom: 10px;
  padding: 7px 14px;
`

type Props = {}

export default class ErrorBoundry extends Component {
  constructor(props: Props) {
    super(props)
    this.state = {error: null, eventId: null}
  }

  props: Props

  componentDidCatch(error, errorInfo) {
    this.setState({error})
    Sentry.withScope((scope) => {
      scope.setExtras(errorInfo)
      const eventId = Sentry.captureException(error)
      this.setState({eventId})
    })
  }

  render() {
    if (this.state.error) {
      //render fallback UI
      return (
        <>
          {this.props.children}
          <Overlay>
            <View>
              <ErrorLabel>Error {this.state.eventId} logged</ErrorLabel>
              <FeedbackContainer
                href="#"
                onPress={() =>
                  Sentry.showReportDialog({eventId: this.state.eventId})
                }>
                <FeedbackLabel>Add feedback</FeedbackLabel>
              </FeedbackContainer>
            </View>
          </Overlay>
        </>
      )
    } else {
      //when there's not an error, render children untouched
      return this.props.children
    }
  }
}
