// @flow

import React, {Component} from 'react'
import {autobind} from 'core-decorators'
import {Link, useRouting} from 'expo-next-react-navigation'
import {connect} from 'react-redux'
import {bindActionCreators} from 'redux'
import styled from 'styled-components/native'
import {Text, View} from 'react-native'

import Icon from 'components/common/Icon'

import {logout} from 'store/modules/auth'
import {clearPlatforms} from 'store/modules/platforms'

import print from 'components/labels'

type GitlabButtonProps = {
  clearPlatforms: () => Promise<{data: Object}>,
  router: Object | String,
  user: Object,
  getMe: Function,
  onPress: Function,
}

type Props = {
  icon: String,
  to: String,
  onClick: Function,
  label: String,
  children: Component,
  shallow: Boolean,
  style: Object,
  outline: Boolean,
}

const ButtonContainer = styled(Link)`
  flex-direction: row;
  justify-content: center;
  align-items: center;
  display: flex;
`

// display: block;
// font-family: BooCity;

const ButtonLabel = styled.Text`
  font-family: lotus;
  font-size: 16px;
  text-shadow: 2px 2px 0px black;
  color: ${({color}) => color};
`

const HeaderButton = (props: Props) => {
  const router = useRouting()
  const color = props.color || '#447abb'
  const {
    routeName,
    icon,
    to,
    onClick,
    label,
    children,
    shallow,
    style,
    outline,
    isPixelated,
  } = props

  const mrProps = {
    web: props.web,
    params: props.params || props.query,
    routeName: routeName,
    style: {flexDirection: 'column', display: 'flex'},
  }

  return (
    <View style={{flexDirection: 'column', alignItems: 'center'}}>
      <ButtonContainer {...mrProps}>
        <Icon
          isPixelated={isPixelated}
          color={color}
          icon={icon}
          size={30}
          style={{flexDirection: 'column', display: 'flex'}}
        />
      </ButtonContainer>
      <ButtonContainer {...mrProps}>
        <ButtonLabel
          color={color}
          style={{flexDirection: 'column', display: 'flex'}}
        >
          {children || label}
        </ButtonLabel>
      </ButtonContainer>
    </View>
  )
}

@connect(
  (state) => ({
    user: state.auth.data,
  }),
  (dispatch) =>
    bindActionCreators(
      {
        clearPlatforms,
      },
      dispatch,
    ),
)
export class Gitlab extends Component<GitlabButtonProps> {
  render() {
    return (
      <HeaderButton
        outline
        color="#cc361e"
        icon="git"
        label={print('Gitlab')}
        web={{
          path: 'https://gitlab.com/NoahGray/create-react-once-app',
          target: '_blank',
        }}
      />
    )
  }
}

export class Platforms extends Component {
  render() {
    return (
      <HeaderButton
        web={{
          path: '/',
        }}
        routeName="/feed"
        as="/"
        shallow
        icon="mobile"
        label={print('Recent posts')}
      />
    )
  }
}

type HomeProps = {
  router: Object,
  user: Object,
}

@connect((state) => ({
  user: state.auth.data,
}))
export class Home extends Component<HomeProps> {
  render() {
    const {user} = this.props

    if (user) {
      return (
        <HeaderButton
          web={{
            path: '/user/[user]',
          }}
          params={{user: user.name}}
          as={`/user/${user.name}`}
          icon="folder"
          label={print('My posts')}
        />
      )
    }

    return null
  }
}

type LogoutProps = {
  router: Object,
  logout: () => Promise,
  user?: Object,
}

@connect(
  (state) => ({
    user: state.auth.data,
  }),
  (dispatch) =>
    bindActionCreators(
      {
        logout,
      },
      dispatch,
    ),
)
export class Logout extends Component<LogoutProps> {
  render() {
    const {router, user, logout} = this.props
    const isLoggedIn = !!user

    return !isLoggedIn ? (
      <HeaderButton
        web={{
          path: router.pathname || '/',
        }}
        params={{login: true, origin: '/'}}
        routeName="/login"
        icon="user-plus"
        label={print('Login')}
      />
    ) : null
    /* <HeaderButton onClick={logout} icon="LogOut" label="Logout" /> */
  }
}

type ProfileProps = {
  router: Object,
  user?: Object,
}

@connect((state) => ({
  user: state.auth.data,
}))
export class Profile extends Component<ProfileProps> {
  render() {
    const {router, user} = this.props

    return user ? (
      <HeaderButton
        web={{
          path: router.pathname,
          query: {profile: true, origin: '/'},
        }}
        shallow
        isPixelated
        icon="user"
      />
    ) : null
  }
}

type FollowingProps = {
  router: Object,
  user?: Object,
}

@connect((state) => ({
  user: state.auth.data,
}))
export class Following extends Component<FollowingProps> {
  render() {
    const {user} = this.props

    return user ? (
      <HeaderButton
        web={{
          path: '/user/[user]/following',
        }}
        params={{user: user.name}}
        as={`/user/${user.name}/following`}
        isPixelated
        icon="at"
        label={print('Following')}
      />
    ) : null
  }
}

export class Twitter extends Component {
  render() {
    return (
      <HeaderButton
        web={{
          path: '/404',
          target: '_blank',
        }}
        isPixelated
        icon="book"
        label={print('Updates')}
      />
    )
  }
}
