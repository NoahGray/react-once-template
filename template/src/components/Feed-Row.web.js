// @flow

import React, {useState} from 'react'
import Link from 'next/link'
import styled from 'styled-components/native'
import {View, Text} from 'react-native'

import {Column, Row} from './common/Flexbox'

import {NEXT_PUBLIC_STORAGE_ENDPOINT, OBJ_STORE_URL} from 'react-native-dotenv'

type Props = {
  key: any,
  style?: Object,
  dream: Object,
  url?: Object,
}

const Container = styled.View`
  display: flex;
  flex-direction: column;
  padding: 9px 12px;
  background-color: #2d2d2d;
  box-sizing: border-box;
  color: white;
  align-items: center;
`

const ProfilePicture = styled.View`
  height: 48px;
  min-width: 48px;
  background-image: ${({imageUrl}) => (imageUrl ? `url(${imageUrl})` : 'grey')};
  background-size: cover;
  background-position: center;
  margin-right: 12px;
  border-radius: 50%;
`

const DisplayName = styled.Text`
  font-family: lotus;
  font-size: 16pt;
  color: white;
`

const Dream = styled.Image`
  height: 256px;
  width: 384px;
`

const RowRenderer = ({dream, style, url}: Props) => {
  const [loaded, setLoaded] = useState(false)
  /* We want relative image paths without forward slashes. This is becuase our data storage engine doesn't like slashes in the keys. So we store them as string keys. Some old values may have forward slashes, so we remove them. */
  let avatarUrl = dream.user.avatar || ''
  avatarUrl = avatarUrl.includes('http')
    ? dream.user.avatar
    : `${NEXT_PUBLIC_STORAGE_ENDPOINT}/${dream.user.avatar}`
  const {name: username} = dream.user

  const imageUri = `/${dream.urls.x8}`

  return (
    <Container style={style}>
      <Column style={{maxWidth: 384}}>
        <Row>
          <ProfilePicture imageUrl={avatarUrl} />
          <Column
            style={{
              color: '#437ABC',
              justifyContent: 'space-between',
              height: '100%',
            }}
          >
            <DisplayName>{dream.user.displayName}</DisplayName>
            <Link
              href={{pathname: '/user/[user]', query: {user: username}}}
              as={`/user/${username}`}
            >
              <Text style={{color: '#CD351E'}}>@{dream.user.name}</Text>
            </Link>
          </Column>
        </Row>
        <Column style={{alignItems: 'center'}}>
          <Dream source={{uri: imageUri}} />
        </Column>
      </Column>
    </Container>
  )
}

export default RowRenderer
