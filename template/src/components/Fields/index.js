// @flow

import React from 'react'
import {Text, TextInput, View} from 'react-native'
import {capitalize} from 'lodash'
import styled from 'styled-components/native'

import print from 'components/labels'

type CommonProps = {
  children: React$Element,
  input: object,
  meta?: object,
  className?: string,
  id?: string,
}

type TextFieldProps = {
  ...CommonProps,
  label: string,
  beforeElement?: any,
  onKeyUp?: func,
}

type RadioProps = {
  ...CommonProps,
  options: array,
}

type TextareaProps = {
  ...CommonProps,
  label: string,
}

const classes = {
  label: {
    padding: '16px 51px',
    border: '1px solid rgb(136, 136, 136)',
  },
  checkbox: {
    display: 'none',
  },
  checkboxChecked: {
    display: 'none',
  },
  textFieldContainer: {
    display: 'flex',
    flexDirection: 'column',
    color: 'black',
  },
}

classes.labelChecked = {
  borderColor: '#0070c9',
  borderWidth: '2px',
}

const Checkbox = ({
  id,
  children,
  input,
  meta: {touched, error},
}: CommonProps) => {
  const showError = touched && error

  return (
    <View id={id} className="reduxField">
      <View
        style={input.value ? classes.labelChecked : classes.label}
        htmlFor={`Checkbox_${input.name}`}
      >
        <TextInput
          {...input}
          id={`Checkbox_${input.name}`}
          style={input.value ? classes.checkboxChecked : classes.checkbox}
          type="checkbox"
        />
        <Text style={classes.text}>{children}</Text>
      </View>
      {showError && <Text>{error}</Text>}
    </View>
  )
}

// const Radio = ({
//   id,
//   className,
//   checkedClassName,
//   options,
//   input,
//   meta: {touched, error},
// }: RadioProps) => {
//   const showError = touched && error
//
//   return (
//     <div id={id} className="reduxField">
//       {options.map((option, index) => {
//         const radioId = `Radio_${option}`
//         const isChecked = input.value === option
//
//         return (
//           <div key={index} className={className}>
//             <label
//               htmlFor={radioId}
//               className={isChecked ? checkedClassName : ''}>
//               <input
//                 {...input}
//                 id={radioId}
//                 type="radio"
//                 value={option}
//                 name={input.name}
//               />
//               <span style={classes.text}>{capitalize(option)}</span>
//             </label>
//             {showError && <span style={classes.error}>{error}</span>}
//           </div>
//         )
//       })}
//     </div>
//   )
// }

const FieldLabel = styled.Text`
  font-family: lotus;
  color: white;
  font-size: 20;
  margin-top: 15px;
  margin-bottom: 10px;
`

function TextField(props: TextFieldProps) {
  const {
    id,
    type,
    label,
    input,
    meta,
    meta: {touched, error},
    style,
  } = props
  const showError = touched && error
  const fieldId = `TextField_${input.name}`

  return (
    <View id={id} className="reduxField">
      <FieldLabel>{print(label)}</FieldLabel>
      <View style={{display: 'flex', width: '100%', alignItems: 'center'}}>
        {meta.active && props.beforeElement}
        <TextInput
          ref={(ref) => (window[`ref${id}`] = ref)}
          style={style}
          {...input}
          onKeyUp={props.onKeyUp}
          type={type || 'text'}
          id={fieldId}
        />
      </View>
      {showError && (
        <View>
          <Text>{error}</Text>
        </View>
      )}
    </View>
  )
}

// const Textarea = ({
//   id,
//   label,
//   input,
//   meta: {touched, error},
// }: TextareaProps) => {
//   const showError = touched && error
//   const fieldId = `Textarea_${input.name}`
//
//   return (
//     <div id={id} className="reduxField">
//       <label style={classes.text} htmlFor={fieldId}>
//         {label}
//       </label>
//       <textarea {...input} id={fieldId}>
//         {input.value}
//       </textarea>
//       {showError && <span style={classes.error}>{error}</span>}
//     </div>
//   )
// }

export {Checkbox, TextField}
