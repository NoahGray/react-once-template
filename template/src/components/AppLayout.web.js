// @flow

import React, {Component} from 'react'

import {connect} from 'react-redux'
import {bindActionCreators} from 'redux'
import Head from 'next/head'
import styled from 'styled-components/native'
import AsyncStorage from '@callstack/async-storage'

import {getMe, logout} from 'store/modules/auth'

import Header from 'components/Header'
import TabBar from 'components/TabBar'
import Window from 'components/common/Window'
import Login from 'components/common/Login'
import ErrorBoundry from 'components/ErrorBoundry'

type Props = {
  url: Object,
  user: Object,
  loaded: boolean,
  getMe: Function,
  logout: Function,
  children: React$Element,
  router: Object,
}

const Container = styled.SafeAreaView`
  flex: 1;
`

const Heading = styled.Text`
  padding: 0;
  margin: 0;
  color: #4079be;
  line-height: 0;
  height: 100%;
  display: flex;
  align-items: center;
`

const ScrollView = styled.ScrollView`
  flex: 2;
`

@connect(
  (state) => ({
    user: state.auth.data,
    loaded: state.auth.loaded,
  }),
  (dispatch) =>
    bindActionCreators(
      {
        getMe,
        logout,
      },
      dispatch,
    ),
)
class Layout extends Component<Props> {
  componentDidMount() {
    if (AsyncStorage.getItem('authToken') && !this.props.loaded) {
      this.props.getMe()
    }
  }

  render() {
    const {
      children,
      router,
      user,
      router: {getParam},
    } = this.props

    const params = ['login', 'register', 'profile', 'reset']
    const [login, register, profile, reset] = params.filter((p) => getParam(p))

    // Can't find a way to just dismiss the modal, which is URL dependant. Annoying as hell.
    const modalDismiss = user
      ? [
          {pathname: '/user/[user]', query: {user: user.name}},
          `/user/${user.name}`,
          {shallow: false},
        ]
      : [{pathname: router.pathname === '/editor' ? '/editor' : '/'}]

    return (
      <Container>
        <Head>
          <meta
            name="viewport"
            content="initial-scale=1.0, width=device-width"
          />
          <meta
            name="google-site-verification"
            content="XJ048YInwQSqVdBs6lXx_OoaiXEVvncEBpxi3g8Y7vo"
          />
          <link rel="icon" type="image/x-icon" href="/static/favicon.ico" />
          {/* <meta name="google-site-verification" content="-FDCc_cIjpjbbUGmpg7KLerq8awrMpfdbXwJPs5SMUo" /> */}
        </Head>
        <Header router={router} />
        {children &&
          React.cloneElement(children, {
            user: this.props.user,
          })}
        <TabBar
          user={this.props.user}
          getMe={getMe}
          logout={logout}
          router={router}
        />
        {(register || login || profile || reset) && (
          <Window dismiss={modalDismiss}>
            <Login registering={!!register} reset={!!reset} router={router} />
          </Window>
        )}
      </Container>
    )
  }
}

const AppLayout = (props) => {
  return (
    <ErrorBoundry>
      <Layout {...props} />
    </ErrorBoundry>
  )
}

export default AppLayout
