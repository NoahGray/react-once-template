// @flow

import React, {Component} from 'react'
import {useRouting} from 'expo-next-react-navigation'
// import { getDreams, clearPlatforms } from "store/modules/dreams"
// import { bindActionCreators } from "redux"
// import wrapper from "store"

import Feed from 'components/Feed'
// import Welcome from 'components/Welcome'

const UserFeed = () => {
  const router = useRouting()
  // const isBlankFeed = !dreams || dreams.length === 0
  // const isOwnFeed = user && pageOfUser === user.name

  return <Feed {...this.props} router={router} />
}

// <>
// {isBlankFeed ? (
//   <Welcome isOwnFeed={isOwnFeed} />
// ) : (
//   <Feed {...this.props} router={router} />
// )}
// </>

export default UserFeed

// export const getServerSideProps = wrapper.getServerSideProps(
//   async ({ store: { dispatch, getState }, query, query: { user } }) => {
//     const actions = bindActionCreators({ clearPlatforms, getDreams }, dispatch)
//
//     await actions.clearPlatforms()
//     const limit = 15
//
//     const args = {
//       skip: query.skip || 0,
//       limit: limit,
//     }
//
//     const dreams = await actions.getDreams({ user, args, following: false })
//
//     return {
//       props: {
//         dreams: dreams.data,
//         count: dreams.meta.count || null,
//       },
//     }
//   }
// )
