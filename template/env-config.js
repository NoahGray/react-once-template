const prod = process.env.NODE_ENV === 'production'

const config = {
  process: {
    env: {
      DEVTOOLS: !prod,
      API_HOST: process.env.API_HOST || 'http://localhost:3001',
      // SENTRY_DSN: process.env.SENTRY_DSN,
      NODE_ENV: process.env.NODE_ENV || 'development',
      NEXT_CDN_PREFIX: process.env.NEXT_CDN_PREFIX,
    },
  },
}

module.exports = config
