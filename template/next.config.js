require('dotenv').config()
const {withExpo} = require('@expo/next-adapter')
const withFonts = require('next-fonts')
const withImages = require('next-images')
const withTM = require('next-transpile-modules')
const withPlugins = require('next-compose-plugins')

const {NEXT_CDN_PREFIX, NODE_ENV, API_HOST} = process.env

const prod = NODE_ENV === 'production'

const baseConfig = withPlugins([
  [
    withTM,
    {
      transpileModules: ['expo-next-react-navigation'],
    },
  ],
  withFonts,
  withImages,
  [
    withExpo,
    {
      projectRoot: __dirname,
    },
  ],
])

baseConfig.assetPrefix = prod ? NEXT_CDN_PREFIX : ''

baseConfig.env = {
  DEVTOOLS: !prod,
  API_HOST: API_HOST || 'http://localhost:3001',
  NEXT_CDN_PREFIX: NEXT_CDN_PREFIX,
}

module.exports = baseConfig
